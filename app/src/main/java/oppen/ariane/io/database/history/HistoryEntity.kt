package oppen.ariane.io.database.history

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "history")
class HistoryEntity(
    @ColumnInfo(name = "uri") val uri: String?,
    @ColumnInfo(name = "timestamp") val timestamp: Long?
){
    @PrimaryKey(autoGenerate = true)
    var uid: Int = 0
}