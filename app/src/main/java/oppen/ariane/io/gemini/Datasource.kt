package oppen.ariane.io.gemini

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import android.content.Context
import oppen.ariane.io.GemState
import oppen.ariane.io.database.history.ArianeHistory
import java.net.URI

interface Datasource {
    fun request(address: String, onUpdate: (state: GemState) -> Unit)
    fun request(address: String, forceDownload: Boolean, onUpdate: (state: GemState) -> Unit)
    fun canGoBack(): Boolean
    fun goBack(onUpdate: (state: GemState) -> Unit)

    companion object{
        fun factory(context: Context, history: ArianeHistory): Datasource {
            return GeminiDatasource(context, history)
        }
    }
}