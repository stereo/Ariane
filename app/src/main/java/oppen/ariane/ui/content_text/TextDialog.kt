package oppen.ariane.ui.content_text

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import android.content.Context
import android.view.View
import androidx.appcompat.app.AppCompatDialog
import kotlinx.android.synthetic.main.dialog_content_text.view.*
import oppen.ariane.R
import oppen.ariane.io.GemState

object TextDialog {

    fun show(context: Context, state: GemState.ResponseText){
        val dialog = AppCompatDialog(context, R.style.AppTheme)

        val view = View.inflate(context, R.layout.dialog_content_text, null)
        dialog.setContentView(view)

        view.text_content.text = state.content

        view.close_text_content_dialog.setOnClickListener {
            dialog.dismiss()
        }


        dialog.show()
    }
}