/**
 * Automatically generated file. DO NOT MODIFY
 */
package oppen.ariadne;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "oppen.ariadne";
  public static final String BUILD_TYPE = "debug";
}
