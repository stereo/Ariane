![Ariane](./assets/web_175x175.png)

# Ariane

A Gemini Protocol browser for Android based OS

## Current

A significant rewrite is under way, check the [Ariane 3/Ariadne](https://codeberg.org/oppenlab/Ariane3) for updates.

## Releases

* Install via direct download from the project page on oppen.digital: [oppen.digital/software/ariane/](https://oppen.digital/software/ariane/)
* Install on [Google Play](https://play.google.com/store/apps/details?id=oppen.gemini.ariane)

# Contributing

## URI/Path issues

If you find a bug while browsing Geminispace there's a good chance it's Ariane's path handling which has had persistent issues since launch. Either [raise an issue here](https://codeberg.org/oppenlab/Ariane/issues), or if you're a developer try adding a test case to the [OppenURI project](https://codeberg.org/oppenlab/OppenURI) (which Ariane uses to resolve navigation).

## Localisation

To add support for another language copy [strings.xml](https://codeberg.org/oppenlab/Ariane/src/branch/main/app/src/main/res/values/strings.xml) and replace the default english with suitable translations. You can do this simply by sending me an amended copy of strings.xml (ariane@fastmail.se) or by cloning the project and submitting a pull request with the full implementation.

More information: [developer.android.com/guide/topics/resources/localization#creating-alternatives](https://developer.android.com/guide/topics/resources/localization#creating-alternatives)

## Issue tracker

[https://codeberg.org/oppenlab/Ariane/issues](codeberg.org/oppenlab/Ariane/issues)

## Licence

[European Union Public Licence v. 1.2](LICENSE)